#!/usr/bin/env python3

class Ring(object):
    def __init__(self):
        self.marbles = [0]
        self.current = 0

    def __repr__(self):
        output = ""
        for i, marble in enumerate(self.marbles):
            output += '{}{}{}'.format('(' if i == self.current else ' ', marble, ')' if i == self.current else ' ') 
        return output

    def insert(self, marble):
        pos = (self.current + 2) % len(self.marbles)
        self.marbles.insert(pos, marble)
        self.current = pos

    def special(self, marble, player):
        scores[player] += marble
        pos = (self.current - 7) % len(self.marbles)
        scores[player] += self.marbles[pos]
        self.marbles.pop(pos)
        self.current = pos

with open("input.txt") as f:
    words = f.readline().strip().split()
    (players, marbles) = (int(words[0]), int(words[6]))

scores = [0] * players
r = Ring()
player = 0

for marble in range(1, marbles+1):
    if marble % 23 == 0:
        r.special(marble, player)
    else:
        r.insert(marble)

    #print("[{}] {}".format(player, r))
    player = (player + 1) % players 

print(max(scores))
