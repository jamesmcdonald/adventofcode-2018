#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct node {
    int64_t value;
    struct node *next;
    struct node *prev;
};

struct node *insert(struct node *current, int64_t marble) {
    struct node *n = (struct node *)malloc(sizeof(struct node));
    n->value = marble;
    struct node *p = current->next;
    n->next = p->next;
    p->next->prev = n;
    n->prev = p;
    p->next = n;
    return n;
}

void dump(struct node *node) {
    struct node *p;
    printf("(%ld)", node->value);
    for(p=node->next; p!=node; p=p->next) {
        printf(" %ld ", p->value);
    }
    printf("\n");
}

struct node *special(struct node *current, int64_t marble, int64_t *score) {
    struct node *p = current->prev->prev->prev->prev->prev->prev->prev;
    struct node *n = p->next;
    *score += marble;
    p->prev->next = p->next;
    p->next->prev = p->prev;
    *score += p->value;
    free(p);
    return n;
}

int main(int argc, char *argv[]) {
    int64_t players;
    int64_t marbles;
    struct node *ring;

    ring = (struct node *)malloc(sizeof(struct node));
    ring->value = 0;
    ring->next = ring;
    ring->prev = ring;

    FILE *f = fopen("input.txt", "r");
    fscanf(f, "%ld players; last marble is worth %ld points", &players, &marbles);
    fclose(f);

    int64_t *scores = (int64_t *)calloc(players, sizeof(int64_t));

    int64_t player = 0;
    for(int64_t marble=1; marble<=marbles; marble++) {
        if (marble % 23 == 0) {
            ring = special(ring, marble, &scores[player]);
        } else {
            ring = insert(ring, marble);
        }
        player = (player + 1) % players;
    }
    int64_t max = 0;
    for (int64_t i=0; i<players; i++) {
        if (scores[i] > max) {
            max = scores[i];
        }
    }
    printf("%ld\n", max);
}
