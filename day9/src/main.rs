extern crate regex;

use std::fs::File;
use std::io::prelude::*;
use regex::Regex;
use std::collections::VecDeque;

fn readinput(filename: &str) -> (usize, u64) {
    let mut f = File::open(filename).unwrap();
    let mut contents = String::new();
    f.read_to_string(&mut contents).unwrap();
    let re = Regex::new(r"(?P<players>\d+) players; last marble is worth (?P<marbles>\d+) points").unwrap();
    for cap in re.captures_iter(&contents.trim()) {
        let players = cap["players"].parse::<usize>().unwrap();
        let marbles = cap["marbles"].parse::<u64>().unwrap();
        return (players, marbles);
    }
    return (0, 0);
}

fn main() {
    let mut ring = VecDeque::new();
    ring.push_back(0);
    let (players, marbles) = readinput("input.txt");
    let mut scores = vec![0; players];

    let mut player = 0;
    for marble in 1..marbles+1 {
        if marble % 23 == 0 {
            for _ in 0..7 {
                let temp = ring.pop_back().unwrap();
                ring.push_front(temp);
            }
            scores[player] += marble + ring.pop_back().unwrap();
            let temp = ring.pop_front().unwrap();
            ring.push_back(temp);
        } else {
            let temp = ring.pop_front().unwrap();
            ring.push_back(temp);
            ring.push_back(marble);
        }
        player = (player + 1) % players;
    }
    let mut max = 0;
    for player in 0..players {
        if scores[player] > max {
            max = scores[player];
        }
    }
    println!("{:?}", max);
}
