#!/usr/bin/env python3

class Node(object):
    def __init__(self, value, next_=None, prev=None):
        self.value = value
        self.next = next_
        self.prev = prev

class Ring(object):
    def __init__(self):
        self.current = Node(0)
        self.current.next = self.current
        self.current.prev = self.current

    def __repr__(self):
        output = ""
        for i, marble in enumerate(self.marbles):
            output += '{}{}{}'.format('(' if i == self.current else ' ', marble, ')' if i == self.current else ' ') 
        return output

    def insert(self, marble):
        p = self.current.next
        n = Node(marble, p.next, p)
        p.next.prev = n
        p.next = n

    def special(self, marble, player):
        scores[player] += marble
        p = self.current.prev.prev.prev.prev.prev.prev.prev
        scores[player] += p.value
        self.current = p.next
        p.prev.next = p.next
        p.next.prev = p.prev
        del p

with open("input.txt") as f:
    words = f.readline().strip().split()
    (players, marbles) = (int(words[0]), int(words[6]))

scores = [0] * players
r = Ring()
player = 0

for marble in range(1, marbles+1):
    if marble % 23 == 0:
        r.special(marble, player)
    else:
        r.insert(marble)

    #print("[{}] {}".format(player, r))
    player = (player + 1) % players 

print(max(scores))
