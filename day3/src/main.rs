use std::fs::File;
use std::io::prelude::*;
use std::cmp;
use std::collections::HashSet;

extern crate regex;

use regex::Regex;

#[derive(Debug)]
struct Rect {
    top: i32,
    left: i32,
    width: i32,
    height: i32,
}

#[derive(Debug)]
struct Claim {
    id: i32,
    rect: Rect,
}

#[derive(Hash, Eq, PartialEq, Debug)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Debug)]
struct PixelGun<'a> {
    r: &'a Rect,
    p: Point,
}

impl<'a> Iterator for PixelGun<'a> {
    type Item = Point;

    fn next(&mut self) -> Option<Point> {
        if self.p.y >= self.r.height {
            return None
        }
        let p = Point { x: self.r.left + self.p.x, y: self.r.top + self.p.y };
        self.p.x += 1;
        if self.p.x >= self.r.width {
            self.p.y += 1;
            self.p.x = 0;
        }
        Some(p)
    }
}

impl Claim {
    fn overlap(&self, other: &Claim) -> Option<Rect> {
        self.rect.overlap(&other.rect)
    }
}

impl Rect {
    fn overlap(&self, other: &Rect) -> Option<Rect> {
        let top = cmp::max(self.top, other.top);
        let bottom = cmp::min(self.top + self.height, other.top + other.height);
        let left = cmp::max(self.left, other.left);
        let right = cmp::min(self.left + self.width, other.left + other.width);

        if left < right && top < bottom {
            //println!("{:?} {:?} {} {} {} {}", &self, &other, top, bottom, left, right);
            Some(Rect {left: left, top: top, width: right - left, height: bottom - top})
        } else {
            None
        }
    }

    /*    fn area(&self) -> i32 {
          self.width * self.height
          }
          */

    fn pixels(&self) -> PixelGun {
        PixelGun { r: self, p: Point { x: 0, y: 0 } }
    }
}

fn readinput(filename: &str) -> Vec<Claim> {
    let mut f = File::open(filename).expect("failed to open");
    let mut contents = String::new();
    f.read_to_string(&mut contents).expect("explosions reading file");
    let re = Regex::new(r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").unwrap();

    contents.trim().split('\n').map(|x| {
        let caps = re.captures(x).unwrap();
        Claim {
            id: caps[1].parse::<i32>().unwrap(),
            rect: Rect {
                left: caps[2].parse::<i32>().unwrap(),
                top: caps[3].parse::<i32>().unwrap(),
                width: caps[4].parse::<i32>().unwrap(),
                height: caps[5].parse::<i32>().unwrap(),
            },
        }

    }).collect()
}

fn main() {
    let input = readinput("input.txt");
    let mut gotunique = false;
    let mut h: HashSet<Point> = HashSet::new();

    // This is not a very efficient way to match the overlaps (every overlap will
    // be found twice), but it lets us scan for the unique claim in the same loop
    for claim in &input {
        let mut unique = true;
        for other in 0..input.len() {
            if claim.id == input[other].id {
                continue
            }
            if let Some(r) = claim.overlap(&input[other]) {
                unique = false;
                for p in r.pixels() {
                    h.insert(p);
                }
            }
        }
        if unique && !gotunique {
            println!("unique claim {}", claim.id);
            gotunique = true;
        }
    }

    println!("{:?}", h.len());

}
