#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>

#define WIDTH 151
#define HEIGHT 150

const char *directions = "^>v<";
    
typedef enum {
    turnleft = -1,
    gostraight = 0,
    turnright = 1,
} turn;

struct cart {
    char dir;
    int loc;
    turn next; 
    int crashed;
};


void dumpgrid(char *grid, struct cart *carts, int cartcount) {
    static char printbuffer[WIDTH * HEIGHT + 1];
    printbuffer[WIDTH * HEIGHT] = 0;
    memcpy(printbuffer, grid, WIDTH * HEIGHT);
    for (int i=0; i<cartcount; i++) {
        printbuffer[carts[i].loc] = carts[i].dir;
    }
    printf("%s", printbuffer);
}

struct point {
    int x;
    int y;
};

int loc(int x, int y) {
    return y * WIDTH + x;
}

struct point invloc(int loc) {
    return (struct point){ loc % WIDTH, loc / WIDTH };
}

char mapcart(char cart) {
    switch (cart) {
        case '>':
            return '-';
        case '<':
            return '-';
        case '^':
            return '|';
        case 'v':
            return '|';
    }
}

int cartmove(const char *grid, struct cart *pc) {
    struct cart c = *pc;
    switch (grid[c.loc]) {
        case '-':
            if (c.dir == '>') c.loc++;
            else if (c.dir == '<') c.loc--;
            break;
        case '|':
            if (c.dir == '^') c.loc -= WIDTH;
            else if (c.dir == 'v') c.loc += WIDTH;
            break;
        case '/':
            switch (c.dir) {
                case 'v':
                    c.loc--;
                    c.dir = '<';
                    break;
                case '^':
                    c.loc++;
                    c.dir = '>';
                    break;
                case '<':
                    c.loc += WIDTH;
                    c.dir = 'v';
                    break;
                case '>':
                    c.loc -= WIDTH;
                    c.dir = '^';
                    break;
            }
            break;
        case '\\':
            switch (c.dir) {
                case '^':
                    c.loc--;
                    c.dir = '<';
                    break;
                case 'v':
                    c.loc++;
                    c.dir = '>';
                    break;
                case '>':
                    c.loc += WIDTH;
                    c.dir = 'v';
                    break;
                case '<':
                    c.loc -= WIDTH;
                    c.dir = '^';
                    break;
            }
            break;
        case '+':
            c.dir = directions[((strchr(directions, c.dir) - directions + 4) + c.next) % 4];
            switch (c.next) {
                case turnleft:
                    c.next = gostraight;
                    break;
                case gostraight:
                    c.next = turnright;
                    break;
                case turnright:
                    c.next = turnleft;
                    break;
            }
            switch (c.dir) {
                case '<':
                    c.loc--;
                    break;
                case '>':
                    c.loc++;
                    break;
                case '^':
                    c.loc -= WIDTH;
                    break;
                case 'v':
                    c.loc += WIDTH;
                    break;
            }
            break;
    }
    *pc = c;
}

int compcart(struct cart *a, struct cart *b) {
    return a->loc > b->loc;
}

int main() {
    char *grid;
    struct cart *carts = (struct cart *)malloc(100 * sizeof(struct cart));
    int cartcount = 0;
    int f = open("input.txt", O_RDONLY);
    grid = mmap(NULL, WIDTH*HEIGHT, PROT_READ|PROT_WRITE, MAP_PRIVATE, f, 0);
    for (int i=0; i<HEIGHT; i++) {
        for (int j=0; j<WIDTH; j++) {
            int l = loc(j,i);
            switch (grid[l]) {
                case '>':
                case '<':
                case '^':
                case 'v':
                    carts[cartcount].dir = grid[l];
                    carts[cartcount].next = turnleft;
                    carts[cartcount].crashed = 0;
                    carts[cartcount++].loc = l;
                    grid[l] = mapcart(grid[l]);
            }
        }
    }

    int crash = 0;
    struct point p;
    for (int tick=0; cartcount > 1; tick++) {
        qsort(carts, cartcount, sizeof(struct cart), (int (*)(const void *, const void *))compcart);
        for (int j=0; j<cartcount; j++) {
            if (carts[j].crashed) continue;
            //printf("%d at %c %d", j, grid[carts[j].loc], carts[j].loc);
            cartmove(grid, &carts[j]);
            //printf(" moved %c to %d\n", carts[j].dir, carts[j].loc);
            for (int k=0; k < cartcount; k++) {
                if (j != k && carts[j].loc == carts[k].loc) {
                    crash = 1;
                    p = invloc(carts[j].loc);
                    printf("crash at tick %d: %d, %d\n", tick, p.x, p.y);
                    carts[j].crashed = 1;
                    carts[k].crashed = 1;
                }
            }
        }

        /* squeeze out the crashed carts */
        if (crash) {
            crash = 0;
            int pos = 0;
            for (int j=0; j<cartcount; j++) {
                if (carts[j].crashed) {
                    //printf("%d is borked\n", j);
                    continue;
                }
                //printf("shuffle %d to %d\n", j, pos);
                carts[pos++] = carts[j];
            }
            cartcount = pos;
        }
        
        /* render the grid in glorious ascii */
        //dumpgrid(grid, carts, cartcount);
    }

    p = invloc(carts->loc);
    printf("one magical cart remains at %d, %d\n", p.x, p.y);
}

