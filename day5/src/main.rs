use std::fs::File;
use std::io::prelude::*;

fn readinput(filename: &str) -> String {
    let mut f = File::open(filename).unwrap();
    let mut contents = String::new();
    f.read_to_string(&mut contents).unwrap();
    contents.trim().to_string()
}

fn reduce(input: &mut String) {
    let mut prev = '@';
    let mut pos = 0;
    let mut remove = false;

    for c in input.chars() {
        if prev != c && prev.to_ascii_lowercase() == c.to_ascii_lowercase() {
            // Stop scanning and start again on next loop
            remove = true;
            break;
        } else {
            prev = c;
            pos += 1;
        }
    }
    if remove {
        let first = input.remove(pos-1);
        let second = input.remove(pos-1);
        //println!("remove {}{} {}", first, second, pos);
    }
}

fn strip(input: &String, kill: char) -> String {
    let mut s = String::new();
    for c in input.chars() {
        if c.to_ascii_lowercase() != kill {
            s.push(c);
        }
    }
    s
}

fn main() {
    let input = readinput("input.txt");
    let mut new = input;
    let mut prev = String::new();
    let mut counter = 0;
    while prev != new {
        counter += 1;
        prev = new.clone();
        reduce(&mut new);
    }
    println!("{}", new.len());
    for c in 97..123 {
        println!("{}", char::from(c));
        let input = readinput("input.txt");
        let mut new = strip(&input, char::from(c));
        let mut prev = String::new();
        let mut counter = 0;
        while prev != new {
            counter += 1;
            prev = new.clone();
            reduce(&mut new);
        }
        println!("{}", new.len());
    }

}
