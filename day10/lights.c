#include <stdio.h>
#include <stdlib.h>

struct point {
    int x;
    int y;
    int vx;
    int vy;
};

struct list {
    struct point *points;
    int count;
};

void add(struct point *target) {
    target->x += target->vx;
    target->y += target->vy;
}

struct list *readinput(char *filename) {
    FILE *f = fopen(filename, "r");
    struct list *l;
    l = (struct list *)malloc(sizeof(struct list));
    l->points = NULL;
    l->count = 0;
    while(!feof(f)) {
        l->points = realloc(l->points, (l->count+1) * sizeof(struct point));
        fscanf(f, "position=< %d,  %d> velocity=<%d, %d>\n", 
                &l->points[l->count].x,
                &l->points[l->count].y,
                &l->points[l->count].vx,
                &l->points[l->count].vy);
        l->count++;
    }
    return l;
}

void printpoint(struct point *p) {
    printf("%d, %d <%d, %d>\n", p->x, p->y, p->vx, p->vy);
}

int main() {
    struct list *l;
    l = readinput("input.txt");

/*    for (int i=0; i<20000; i++) {
        int minx = 200000, miny = 200000;
        int maxx = -200000, maxy = -200000;
        for (int j=0; j<l->count; j++) {
            if (l->points[j].x < minx) minx = l->points[j].x;
            if (l->points[j].y < miny) miny = l->points[j].y;
            if (l->points[j].x > maxx) maxx = l->points[j].x;
            if (l->points[j].y > maxy) maxy = l->points[j].y;
            add(&l->points[j]);
        }
        printf("%d: %d %d\n", i, maxx - minx, maxy - miny);
    } */
    
    char buffer[10][63];
    for (int i=0; i<10; i++) {
        for(int j=0; j<63; j++) {
            if (j==62) buffer[i][j]='\0'; else buffer[i][j] = ' ';
        }
    }
    for (int i=0; i<10727; i++) {
        for (int j=0; j<l->count; j++) {
            add(&l->points[j]);
        }
    }
    for (int i=0; i<l->count; i++) {
        printpoint(&l->points[i]);
    }
    for (int i=0; i<l->count; i++) {
        struct point *p=&l->points[i];
        int xfiddle = 110;
        int yfiddle = 135;
        printf("%d %d, ", p->y-yfiddle, p->x-xfiddle);
        fflush(stdout);
        buffer[p->y-yfiddle][p->x-xfiddle] = '#';
    }
    printf("\n");
    for (int i=0; i<10; i++) {
        printf("%s\n", buffer[i]);
    }

}
