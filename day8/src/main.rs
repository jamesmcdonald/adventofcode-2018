use std::fs::File;
use std::io::prelude::*;

struct Node {
    length: usize,
    mdtreesum: i32,
    value: i32,
}

fn readinput(filename: &str) -> Vec<i32> {
    let mut f = File::open(filename).unwrap();
    let mut contents = String::new();
    f.read_to_string(&mut contents).unwrap();
    contents.trim().split_whitespace().map(|s| {
        s.parse::<i32>().unwrap()
    }).collect()
}

fn parsenodeat(input: &Vec<i32>, offset: usize) -> Node {
    let mut length = 2;
    let mut mdtreesum = 0;
    let chcount = input[offset];
    let mdcount = input[offset+1];
    let mut children = Vec::new();
    let mut value = 0;

    for _ in 0..chcount {
        let child = parsenodeat(&input, offset+length);
        length += child.length;
        mdtreesum += child.mdtreesum;
        children.push(child);
    }

    let mut metadata = Vec::new();
    for _ in 0..mdcount {
        let md = input[offset+length];
        metadata.push(md);
        mdtreesum += md;
        length += 1;
        if chcount == 0 {
            value += md;
        } else {
            if children.len() >= md as usize {
                value += children[md as usize - 1].value;
            }
        }
    }
    println!("Node parsed at {}: {}, {}, {}, {}, {:?}", offset, chcount, mdcount, mdtreesum, value, metadata);
 
    Node { length, mdtreesum, value }
}

fn main() {
    let input = readinput("input.txt");
    parsenodeat(&input, 0);
}
