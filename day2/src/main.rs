use std::fs::File;
use std::io::prelude::*;
use std::collections::HashMap;

#[derive(Debug)]
struct Counts {
    twos: i32,
    threes: i32,
}

fn readinput(filename: &str) -> Vec<String> {
    let mut f = File::open(filename).expect("failed to open");
    let mut contents = String::new();
    f.read_to_string(&mut contents).expect("explosions reading file");

    let v = contents.trim().split('\n').map(|x| { x.to_string() }).collect();
    v
}

fn count(s: &str) -> Counts {
    let mut twos = 0;
    let mut threes = 0;
    let mut h: HashMap<char, i32> = HashMap::new();
    for c in s.chars() {
        if !h.contains_key(&c) {
            h.insert(c, 1);
        } else {
            let newval = h.get(&c).unwrap() + 1;
            h.insert(c, newval);
        }
    }
    for (c, _) in &h {
        if *(h.get(&c).unwrap()) == 3 {
            threes = 1;
        }
        if *(h.get(&c).unwrap()) == 2 {
            twos = 1;
        }
    }
    Counts { twos, threes }
}

fn onematch(a: &str, b: &str) -> bool {
    let mut diffs = 0;

    for (ac, bc) in a.chars().zip(b.chars()) {
        if ac != bc {
            diffs+=1;
        }
    }
    diffs == 1
}

fn letters(a: &str, b: &str) -> String {
    let mut result = String::new();

    for (ac, bc) in a.chars().zip(b.chars()) {
        if ac == bc {
            result.push(ac);
        }
    }
    result
}
    

fn main() {
    let input = readinput("input.txt");
    let mut twos = 0;
    let mut threes = 0;

    let mut common = String::new();

    for (pos, val) in input.iter().enumerate() {
        let c = count(&val);
        twos += c.twos;
        threes += c.threes;
        if common.len() == 0 {
            for cmp in pos+1..input.len() {
                if onematch(&val, &input[cmp]) {
                    common = letters(&val, &input[cmp]);
                    break;
                }
            }
        }

    }
    println!("{}", twos * threes);
    println!("{}", common);
}
