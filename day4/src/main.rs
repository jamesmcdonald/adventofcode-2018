extern crate regex;
extern crate chrono;

use std::fs::File;
use std::io::prelude::*;
use chrono::{NaiveDateTime, Timelike};
use regex::Regex;
use std::collections::HashMap;

#[derive(Debug)]
struct Event {
    stamp: NaiveDateTime,
    content: String,
}

fn readinput(filename: &str) -> Vec<Event> {
    let mut f = File::open(filename).expect("failed to open");
    let mut contents = String::new();
    f.read_to_string(&mut contents).expect("explosions reading file");
    let re = Regex::new(r"\[(\d+-\d+-\d+ \d+:\d+)\] (.*)$").unwrap();

    contents.trim().split('\n').map(|x| {
        let caps = re.captures(x).unwrap();
        Event {
            stamp: NaiveDateTime::parse_from_str(&caps[1], "%Y-%m-%d %H:%M").unwrap(),
            content: caps[2].to_string()
        }
    }).collect()
}

fn parseevents<I>(input: I)
where
I: IntoIterator<Item = Event> {
    let mut id = 0;
    let mut sleep = 0;
    let mut totals  = HashMap::new();
    let mut sleeptime = HashMap::new();
    //let mut h: Vec<HashMap<i32,i32>> = repeat(HashMap::new()).take(60).collect();
    for event in input {
        if event.content.starts_with("Guard") {
            let digits: String = event.content.split_whitespace().skip(1).take(1).collect::<String>().chars().skip(1).collect();
            id = digits.parse::<u32>().unwrap();
        } else if event.content.starts_with("falls") {
            println!("{} asleep {}", id, event.stamp.minute());
            sleep = event.stamp.minute();
        } else if event.content.starts_with("wakes") {
            println!("{} awake {}", id, event.stamp.minute());
            let total = totals.entry(id).or_insert(0);
            *total += event.stamp.minute() - sleep;
            for x in sleep..event.stamp.minute() {
                let s = sleeptime.entry(id).or_insert(HashMap::new());
                let m = (*s).entry(x).or_insert(0);
                *m += 1;
            }
        }
    }
    println!("{:?}", totals);
    println!("{:?}", sleeptime);
    let mut winner = 0;
    let mut max = 0;
    for (key, val) in totals {
        if val > max {
            max = val;
            winner = key;
        }
    }
    max = 0;
    let mut minute = 0;
    for (key, val) in sleeptime.get(&winner).unwrap() {
        if val > &max {
            max = *val;
            minute = *key;
        }
    }
    println!("solution 1: winner {} minute {} solution {}", winner, minute, winner * minute);

    max = 0;
    winner = 0;
    minute = 0;
    for (guard, hash) in sleeptime {
        for (m, count) in hash {
            if count > max {
                max = count;
                winner = guard;
                minute = m;
            }
        }
    }
    println!("solution 2: winner {} minute {} solution {}", winner, minute, winner * minute);
}

fn main() {
    let mut input = readinput("input.txt");
    input.sort_unstable_by_key(|x| x.stamp);
    println!("{:?}", &input);
    parseevents(input);
}
