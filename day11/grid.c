#include <stdio.h>

#define INPUT 9995

static int grid[300][300];

int main() {
    for (int y = 0; y < 300; y++) {
        for (int x = 0; x < 300; x++) {
            int rackid = (x + 1 + 10);
            int power = (y + 1) * rackid;
            power += INPUT;
            power *= rackid;
            power /= 100;
            power %= 10;
            power -= 5;
            grid[y][x] = power;
        }
    }
    int max = 0;
    int maxx = 0;
    int maxy = 0;
    for (int y = 0; y < 297; y++) {
        for (int x = 0; x < 297; x++) {
            int sum = grid[y][x] + grid[y][x+1] + grid[y][x+2];
            sum += grid[y+1][x] + grid[y+1][x+1] + grid[y+1][x+2];
            sum += grid[y+2][x] + grid[y+2][x+1] + grid[y+2][x+2];
            if (sum > max) {
                max = sum;
                maxx = x;
                maxy = y;
            }
        }
    }
    printf("%d %d\n", maxx+1, maxy+1);

    max = 0;
    maxx = 0;
    maxy = 0;
    int maxsize = 0;

    for (int size=1; size < 299; size++) {
        printf("size %d\n", size);
        for (int y = 0; y < 300 - size; y++) {
            for (int x = 0; x < 300 - size; x++) {
                int sum = 0;
                for (int i=0; i<size; i++) {
                    for (int j=0; j<size; j++) {
                        sum += grid[y+i][x+j];
                    }
                }
                if (sum > max) {
                    max = sum;
                    maxx = x;
                    maxy = y;
                    maxsize = size;
                }
            }
        }
    }
    printf("%d %d %d\n", maxx+1, maxy+1, maxsize);
}
