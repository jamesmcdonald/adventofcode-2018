#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int WORKERS = 5;

struct step {
    char id;
    int done;
    char *requires;
    int finished;
};

int getworker(int *workers) {
    for(int i=0; i<WORKERS; i++) {
        if (workers[i] == 0) {
            return i;
        }
    }
    /* return -1 if no workers are free */
    return -1;
}

int getminbusy(int *workers) {
    /* get the time of the least busy worker */
    int min = 100000;
    for(int i=0; i<WORKERS; i++) {
        if (workers[i] && workers[i] < min) {
            min = workers[i];
        }
    }
    return min == 100000?0:min;
}

int advancetime(int *workers, struct step *steps) {
    /* Advance until the next worker is free */
    int min = getminbusy(workers);
    for (int i=0; i<WORKERS; i++) {
        if (workers[i]) {
            workers[i] -= min;
        }
    }
    for (int i=0; i<26; i++) {
        if (steps[i].finished) {
            steps[i].finished -= min;
            if (steps[i].finished == 0) {
                steps[i].done = 1;
                printf("finish %c\n", steps[i].id);
            }
        }
    }
    return min;
}

int main() {
    FILE *fp;
    struct step steps[26];
    char order[27];
    int workers[WORKERS];

    for(int i=0; i<26; i++) {
        steps[i].id = i+'A';
        steps[i].done = 0;
        steps[i].finished = 0;
        steps[i].requires = (char *)calloc(27, 1);
        order[i] = '\0';
        if (i < WORKERS) {
            workers[i] = 0;
        }
    }
    order[26] = 0;

    fp = fopen("input.txt", "r");
    while(!feof(fp)) {
        char a, b;
        fscanf(fp, "Step %c must be finished before step %c can begin.\n", &a, &b);
        printf("%c %c\n", a, b);
        int step = b - 'A';
        if (!strchr(steps[step].requires, a)) {
            steps[step].requires[strlen(steps[step].requires)] = a;
        }
    }
    fclose(fp);

    int timer = 0;
    for (int undone=26; undone; undone=undone?26:0) {
        int started = 0;
        for(int i=0; i<26; i++) {
            if (steps[i].done) {
                undone--;
            } else if (!steps[i].finished) {
                int reqsundone = strlen(steps[i].requires);
                for(int j=0; j<strlen(steps[i].requires); j++) {
                    reqsundone -= steps[steps[i].requires[j]-'A'].done;
                }
                if (!reqsundone) { /* all reqs are completed */
                    printf("queue %c\n", steps[i].id);
                    int w = getworker(workers);
                    workers[w] += 61 + i;
                    order[strlen(order)] = i + 'A';
                    steps[i].finished = workers[w];
                    started = 1;
                    break;
                }
            }
            printf("%c %d %s\n", steps[i].id, steps[i].done, steps[i].requires);
        }
        if (!started) {
            int adv = advancetime(workers, steps);
            timer += adv;
            printf("advance time by %d to %d\n", adv, timer);
        }
    }

    printf("%s\n", order);
    printf("%d\n", timer);
}
