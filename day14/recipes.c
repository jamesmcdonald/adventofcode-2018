#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

void board_print(int *board, int board_length, int elf0, int elf1) {
    for (int i=0; i<board_length; i++) {
        char a, b;
        if (elf0 == i) {
            a = '('; b = ')';
        } else if (elf1 == i) {
            a = '['; b = ']';
        } else {
            a = ' '; b = ' ';
        }

        printf("%c%d%c", a, board[i], b);
    }
    printf("\n");
}

int main(int argc, char *argv[])
{
    int board_capacity = 64;
    int *board = (int *)malloc(board_capacity * sizeof(int));
    board[0] = 3;
    board[1] = 7;
    int board_length = 2;
    int elf0 = 0;
    int elf1 = 1;

    if (argc != 2) {
        fprintf(stderr, "usage: %s <recipes>\n", argv[0]);
        exit(1);
    }

    int count = atoi(argv[1]);

    //board_print(board, board_length, elf0, elf1);
    for(int i=0; ; i++) {
        int new = board[elf0] + board[elf1];
        int grow = new >= 10 ? 2 : 1;
        if (board_length + grow > board_capacity) {
            board_capacity *= 2;
            board = realloc(board, board_capacity * sizeof(int));
        }
        if (grow == 2) {
            board[board_length++] = new / 10;
        }
        board[board_length++] = new % 10;
        elf0 = (elf0 + board[elf0] + 1) % board_length;
        elf1 = (elf1 + board[elf1] + 1) % board_length;
        if (board_length == count + 10) {
            for(int j=10; j>0; j--) {
                printf("%d", board[board_length-j]);
            }
            printf("\n");
            //board_print(board, board_length, elf0, elf1);
        }
        if (
                (board[board_length - 2] == 1 &&
                board[board_length - 3] == 0 &&
                board[board_length - 4] == 5 &&
                board[board_length - 5] == 4 &&
                board[board_length - 6] == 7 &&
                board[board_length - 7] == 0) ||
                (board[board_length - 1] == 1 &&
                board[board_length - 2] == 0 &&
                board[board_length - 3] == 5 &&
                board[board_length - 4] == 4 &&
                board[board_length - 5] == 7 &&
                board[board_length - 6] == 0)) {
            printf("match at %d\n", board_length);
            break;
        }
    }
}
