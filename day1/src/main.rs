use std::fs::File;
use std::io::prelude::*;
use std::collections::HashSet;

const FILENAME: &str = "input.txt";

fn main() {
    let mut f = File::open(FILENAME).expect("failed to open");
    let mut contents = String::new();
    f.read_to_string(&mut contents).expect("explosions reading file");

    let v: Vec<i32> = contents.split('\n').map(|x| {
        x.parse::<i32>()
    }).filter(|x| {
        match x {
            Ok(_) => true,
            _ => false,
        }
    }).map(|x| {
        x.unwrap()
    }).collect();

    let mut output_frequency = 0;
    let mut frequency = 0;
    let mut seen = HashSet::new();
    let mut matched = false;
    let mut twice_freq = 0;
    let mut first_run = true;

    while !matched {
        for adjust in &v {
            if !matched {
                seen.insert(frequency);
            }
            frequency += adjust;
            if !matched && seen.contains(&frequency) {
                twice_freq = frequency;
                matched = true;
            }
        }
        if first_run {
            output_frequency = frequency;
            first_run = false;
        }
    }
    println!("{}", output_frequency);
    println!("{}", twice_freq);
}
