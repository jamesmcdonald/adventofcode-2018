#include <stdio.h>
#include <malloc.h>

struct instruction {
    int opcode;
    int a;
    int b;
    int c;
};

void op_addr(struct instruction op, int reg[4]) {
    // addr (add register) stores into register C the result of adding register A and register B.
    reg[op.c] = reg[op.a] + reg[op.b];
}

void op_addi(struct instruction op, int reg[4]) {
    // addi (add immediate) stores into register C the result of adding register A and value B.
    reg[op.c] = reg[op.a] + op.b;
}

// Multiplication:
//
void op_mulr(struct instruction op, int reg[4]) {
    // mulr (multiply register) stores into register C the result of multiplying register A and register B.
    reg[op.c] = reg[op.a] * reg[op.b];
}

void op_muli(struct instruction op, int reg[4]) {
    // muli (multiply immediate) stores into register C the result of multiplying register A and value B.
    reg[op.c] = reg[op.a] * op.b;
}

// Bitwise AND:
//
void op_banr(struct instruction op, int reg[4]) {
    // banr (bitwise AND register) stores into register C the result of the bitwise AND of register A and register B.
    reg[op.c] = reg[op.a] & reg[op.b];
}

void op_bani(struct instruction op, int reg[4]) {
    // bani (bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
    reg[op.c] = reg[op.a] & op.b;
}

// Bitwise OR:
//
void op_borr(struct instruction op, int reg[4]) {
    // borr (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
    reg[op.c] = reg[op.a] | reg[op.b];
}

void op_bori(struct instruction op, int reg[4]) {
    // bori (bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
    reg[op.c] = reg[op.a] | op.b;
}

// Assignment:
//
void op_setr(struct instruction op, int reg[4]) {
    // setr (set register) copies the contents of register A into register C. (Input B is ignored.)
    reg[op.c] = reg[op.a];
}

void op_seti(struct instruction op, int reg[4]) {
    // seti (set immediate) stores value A into register C. (Input B is ignored.)
    reg[op.c] = op.a;
}

// Greater-than testing:
//
void op_gtir(struct instruction op, int reg[4]) {
    // gtir (greater-than immediate/register) sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
    reg[op.c] = op.a > reg[op.b];
}

void op_gtri(struct instruction op, int reg[4]) {
    // gtri (greater-than register/immediate) sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.
    reg[op.c] = reg[op.a] > op.b;
}

void op_gtrr(struct instruction op, int reg[4]) {
    // gtrr (greater-than register/register) sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.
    reg[op.c] = reg[op.a] > reg[op.b];
}

// Equality testing:
//
void op_eqir(struct instruction op, int reg[4]) {
    // eqir (equal immediate/register) sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0.
    reg[op.c] = op.a == reg[op.b];
}

void op_eqri(struct instruction op, int reg[4]) {
    // eqri (equal register/immediate) sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
    reg[op.c] = reg[op.a] == op.b;
}

void op_eqrr(struct instruction op, int reg[4]) {
    // eqrr (equal register/register) sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
    reg[op.c] = reg[op.a] == reg[op.b];
}

void (*ops[])(struct instruction, int[4]) = {
    op_addr, op_addi, op_mulr, op_muli, op_banr, op_bani, op_borr, op_bori, op_setr, op_seti, op_gtir, op_gtri, op_gtrr, op_eqri, op_eqir, op_eqrr, NULL};

char *opnames[] = {
    "op_addr", "op_addi", "op_mulr", "op_muli", "op_banr", "op_bani", "op_borr", "op_bori", "op_setr", "op_seti", "op_gtir", "op_gtri", "op_gtrr", "op_eqri", "op_eqir", "op_eqrr", NULL};

int opcodes[] = {
    8, 12, 0, 5, 9, 7, 6, 15, 2, 14, 11, 13, 4, 1, 10, 3 };

void printop(struct instruction op) {
    printf("%d: %d, %d, %d\n", op.opcode, op.a, op.b, op.c);
}

void printreg(int reg[4]) {
    printf("[%d, %d, %d, %d]\n", reg[0], reg[1], reg[2], reg[3]);
}

int opreader(FILE *in, struct instruction *op, int b[4], int a[4]) {
    char c;
    if ((c = getc(in)) != 'B') {
        ungetc(c, in);
        return 0;
    }
    ungetc(c, in);

    fscanf(in, "Before: [%d, %d, %d, %d]\n", &b[0], &b[1], &b[2], &b[3]);
    fscanf(in, "%d %d %d %d\n", &op->opcode, &op->a, &op->b, &op->c);
    fscanf(in, "After: [%d, %d, %d, %d]\n\n", &a[0], &a[1], &a[2], &a[3]);
    return 1;
}

int main() {
    int reg[4];
    int before[4];
    int after[4];
    struct instruction op;

    FILE *in = fopen("input.txt", "r");

    /* samples which match more than 3 ops */
    int confusers = 0;

    int *opcodematches[16];
    int ocm_len[16];
    for (int i=0; i < 16; i++) {
        opcodematches[i] = NULL;
        ocm_len[i] = 0;
    }

    while(opreader(in, &op, before, after)) {
        printreg(before);
        printop(op);
        printreg(after);
        int matches = 0;
        for(int i=0; ops[i]; i++) {
            reg[0] = before[0];
            reg[1] = before[1];
            reg[2] = before[2];
            reg[3] = before[3];
            (*ops[i])(op, reg);
            if (reg[0] == after[0] &&
                    reg[1] == after[1] &&
                    reg[2] == after[2] &&
                    reg[3] == after[3]) {
                printf("match on %s\n", opnames[i]);
                matches++;
                if (opcodes[i] >= 0) continue;
                ocm_len[op.opcode]++;
                opcodematches[op.opcode] = realloc(opcodematches[op.opcode], ocm_len[op.opcode] * sizeof(int));
                opcodematches[op.opcode][ocm_len[op.opcode]-1] = i;
            }
        }
        if (matches >= 3) {
            confusers++;
        }
    }
    printf("%d\n", confusers);
    for (int i=0; opnames[i]; i++) {
        printf("%s %d\n", opnames[i], opcodes[i]);
    }
    for (int i=0; i<16; i++) {
        printf("opcode %d matches: ", i);
        for (int j=0; j<ocm_len[i]; j++) {
            printf("%d ", opcodematches[i][j]);
        }
        printf("\n");
    }

    while(fscanf(in, "%d %d %d %d\n", &op.opcode, &op.a, &op.b, &op.c) != EOF) {
        for (int i=0; i<16; i++) {
            if (opcodes[i] == op.opcode) {
                (*ops[i])(op, reg);
                continue;
            }
        }
    }
    printreg(reg);

}
