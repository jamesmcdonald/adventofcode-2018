use std::fs::File;
use std::io::prelude::*;
use std::fmt;
use std::collections::HashMap;
use std::collections::HashSet;

#[derive(Debug)]
struct Point {
    x: i32,
    y: i32,
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({},{})", self.x, self.y)
    }
}

fn readinput(filename: &str) -> Vec<Point> {
    let mut f = File::open(filename).unwrap();
    let mut contents = String::new();
    f.read_to_string(&mut contents).unwrap();
    contents.trim().split('\n').map(|s| {
        let coords: Vec<&str> = s.split(", ").collect();
        Point {
            x: coords[0].parse::<i32>().unwrap(),
            y: coords[1].parse::<i32>().unwrap(),
        }
    }).collect()
}

fn manhattan(a: &Point, b: &Point) -> i32 {
    (a.x - b.x).abs() + (a.y - b.y).abs()
}

fn getbounds(points: &Vec<Point>) -> (Point, Point) {
    let mut min = Point { x: 10000, y: 10000 };
    let mut max = Point { x: 0, y: 0 };
    for point in points {
        if point.x < min.x {
            min.x = point.x;
        }
        if point.y < min.y {
            min.y = point.y;
        }
        if point.x > max.x {
            max.x = point.x;
        }
        if point.y > max.y {
            max.y = point.y;
        }
    }
    min.x -= 1;
    min.y -= 1;
    max.x += 1;
    max.y += 1;
    (min, max)
}

fn main() {
    let input = readinput("input.txt");
    let mut output = File::create("output.txt").unwrap();
    println!("{:?}", input);
    let (min, max) = getbounds(&input);
    println!("min {} max {}", min, max);
    let mut area: HashMap<usize, i32> = HashMap::new();
    let mut discard: HashSet<usize> = HashSet::new();
    let mut safezone = 0;

    let chars = b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    for y in min.y..max.y+1 {
        for x in min.x..max.x+1 {
            let p = Point { x, y };
            let mut minm = 10000;
            let mut mp = 0;
            let mut equal = false;
            let mut totalm = 0;
            for (pos, point) in input.iter().enumerate() {
                let m = manhattan(&point, &p);
                totalm += m;
                if m < minm {
                    minm = m;
                    mp = pos;
                    equal = false;
                } else if m == minm {
                    equal = true;
                }
            }
            if totalm < 10000 {
                safezone += 1;
            }
            if min.y == y || min.x == x || max.x == x || max.y == y {
                //println!("{} is on the edge, discard {}", p, input[mp]);
                discard.insert(mp);
            } else if equal {
                //println!("{} is equidistant and discarded", p);
                output.write(b".").unwrap();
            } else {
                let val = area.entry(mp).or_insert(0);
                *val += 1;
                if minm == 0 {
                    output.write(b"*").unwrap();
                } else {
                    let c = &chars[mp..mp+1];
                    output.write(c).unwrap();
                }
            }
        }
        output.write(b"\n").unwrap();
    }

    let mut max = 0;
    for (key, val) in area {
        if discard.contains(&key) {
            continue
        }
        if val > max {
            max = val
        }
    }
    println!("area {}", max);
    println!("safezone {}", safezone);
}
