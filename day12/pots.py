#!/usr/bin/env python3

patterns = {}
with open('input.txt') as infile:
    state = '.' * 1000 + infile.readline().strip().split(': ')[1] + '.' * 1000 
    infile.readline()
    for line in infile.readlines():
        key, value = line.strip().split(' => ')
        patterns[key] = value

prev = 0
for i in range(2000):
    newstate = ['.'] * len(state)
    for j in range(len(state) - 5):
        for key in patterns:
            if state[j:j+5] == key:
                #print(state[j:j+5], "matches", key, "- replace", patterns[key])
                newstate[j+2] = patterns[key]
    state = ''.join(newstate)
#    print(' ' * 60 + '0')
#    print(state)

    pot = -1000
    total = 0
    for c in state:
        if c == '#':
            total += pot
        pot += 1

    print(i, total, total - prev)
    prev = total
